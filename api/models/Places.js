const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PlacesSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  title: {
    type: String,
    require: true
  },
  description: {
    type: String,
    require: true
  },
  image: {
    type: String,
    require: true
  },
  overallRatings: {
    type: Number
  },
  commentsAll: {
    type: String
  },
  imagesAll: {
    type: String
  }
});

const Place = mongoose.model('Place', PlacesSchema);

module.exports = Place;
