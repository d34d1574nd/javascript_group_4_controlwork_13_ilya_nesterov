const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ImagesSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  placeId: {
    type: Schema.Types.ObjectId,
    ref: 'Place',
    required: true
  },
  images: {
    type: String
  }
});

const Image = mongoose.model('Image', ImagesSchema);

module.exports = Image;
