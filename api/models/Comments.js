const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CommentsSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  placeId: {
    type: Schema.Types.ObjectId,
    ref: 'Place',
    required: true
  },
  data: {
    type: String
  },
  message: {
    type: String,
    required: true
  },
  easyMakeRating: {
    type: Number,
    required: true
  },
  quickMakeRating: {
    type: Number,
    required: true
  },
  tasteRatings: {
    type: Number,
    required: true
  }
});

const Comment = mongoose.model('Comment', CommentsSchema);

module.exports = Comment;
