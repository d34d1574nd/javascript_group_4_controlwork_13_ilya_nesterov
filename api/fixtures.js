const mongoose = require('mongoose');
const config = require('./config');
const nanoid = require('nanoid');

const Users = require('./models/User');
const Places = require('./models/Places');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const users = await Users.create({
    username: 'user',
    password: '123',
    role: 'user',
    token: nanoid()
  },{
    username: "Ilya",
    password: '123',
    role: "user",
    token: nanoid()
  }, {
    username: "admin",
    password: '123',
    role: "admin",
    token: nanoid()
  });

  await Places.create(
    {userId: users[0]._id, title: "Бублик", description: "Кофейня нового поколения, где можно хорошо отдохнуть, выпить вкусный кофе и даже поработать за ноутбуком", image: 'bublik.jpeg'},
    {userId: users[0]._id, title: "Бублик", description: "Кофейня нового поколения, где можно хорошо отдохнуть, выпить вкусный кофе и даже поработать за ноутбуком", image: 'bublik.jpeg'},
    {userId: users[0]._id, title: "Бублик", description: "Кофейня нового поколения, где можно хорошо отдохнуть, выпить вкусный кофе и даже поработать за ноутбуком", image: 'bublik.jpeg'},
    )


  return connection.close();
};


run().catch(error => {
  console.error('Something wrong happened...', error);
});
