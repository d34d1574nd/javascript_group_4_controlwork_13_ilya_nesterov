const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');

const users = require('./app/users');
const places = require('./app/places');
const comments = require('./app/comment');
const images = require('./app/imagePlace');
const place = require('./app/place');
const admin = require('./app/admin');

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = process.env.NODE_ENV === 'test' ? 8010 : 8000;

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
  app.use('/users', users);
  app.use('/places', places);
  app.use('/comments', comments);
  app.use('/images', images);
  app.use('/place', place);
  app.use('/admin', admin);

app.listen(port, () => {
  console.log(`Server started on ${port} port`);
});
});
