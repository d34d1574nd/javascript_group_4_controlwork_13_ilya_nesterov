const express = require('express');

const auth = require('../middleware/auth');
const Places = require('../models/Places');

const router = express.Router();

router.post('/:id', [auth], async (req, res) => {
  try {
    await req.body;
    await Places.updateOne({_id: req.params.id},
      {$set:
          {
            overallRatings: req.body.rating,
            commentsAll: req.body.comments,
            imagesAll: req.body.images
          }
      });
    res.send({"message": "ok"})
  } catch (error) {
    console.log(error);
    return res.status(400).send(error)
  }
});



module.exports = router;
