const express = require('express');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const Comment = require('../models/Comments');
const ImagesPlace = require('../models/ImagesPlace');
const Place = require('../models/Places');

const router = express.Router();

router.delete('/comment/:id', [auth, permit('admin')], async (req, res) => {
  await Comment.deleteOne({_id: req.params.id});
  
  return res.send({message: "OK"});
});

router.delete('/image/:id', [auth, permit('admin')], async (req, res) => {
  await ImagesPlace.deleteOne({_id: req.params.id})

  return res.send({message: "OK"});
});

router.delete('/place/:id', [auth, permit('admin')], async (req, res) => {
  await Place.deleteOne({_id: req.params.id});
  await ImagesPlace.deleteMany({placeId: req.params.id})
  await Comment.deleteMany({placeId: req.params.id});

  return res.send({message: "OK"});
});

module.exports = router;
