const express = require('express');
const config = require('../config');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const auth = require('../middleware/auth');
const Comment = require('../models/Comments');
const ImagesPlace = require('../models/ImagesPlace');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.post('/', [auth, upload.array('images', 10)], async (req, res) => {
  try {
    const data = {
      images: '',
      userId: req.user._id,
      placeId: req.body.placeId
    }
    for (let i = 0; i < req.files.length; i++) {
      data.images = req.files[i].filename
      const imagesPlace = new ImagesPlace(data);
      imagesPlace.save()
    }

    const message = {
      data: new Date().toLocaleString("ru-RU"),
      userId: req.user._id,
      placeId: req.body.placeId,
      message: req.body.message,
      easyMakeRating: req.body.easyMakeRating,
      quickMakeRating: req.body.quickMakeRating,
      tasteRatings: req.body.tasteRatings
    };
    const comment = await new Comment(message);
    comment.save();

    return res.send(comment);
  } catch (error) {
    console.log(error);
    return res.status(400).send(error)
  }
});

router.get('/:id', async (req, res) => {
  try {
    const comments = await Comment.find({placeId: req.params.id}).populate('userId')
    return res.send(comments)
  } catch (error) {
    console.log(error);
    return res.status(400).send(error)
  }
});



module.exports = router;
