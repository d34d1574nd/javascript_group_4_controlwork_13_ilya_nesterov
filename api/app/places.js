const express = require('express');
const config = require('../config');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const Places = require('../models/Places');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.post('/', [auth, upload.single('image')], async (req, res) => {
  try {

    const placeData = {
      userId: req.user._id,
      title: req.body.title,
      description: req.body.description
    };

    if (req.file) {
      placeData.image = req.file.filename;
    }

    const place = await new Places(placeData);

    if (req.body.agreement === "false") {
      return res.status(400).send({error: 'Вы не приняли пользовательское соглашение'})
    } else if (req.body.agreement === "true") {
      place.save()
    }

    return res.send(place)
  } catch (error) {
    return res.status(400).send(error)
  }
});


router.get('/', async (req, res) => {
  try {
    const places = await Places.find().populate('user');
    return res.send(places)
  } catch (error) {
    return res.status(400).send(error)
  }
});

router.get('/:id', async (req, res) => {
  try {
    const place = await Places.findById(req.params.id).populate('userId');
    return res.send(place)
  } catch (error) {
    return res.status(400).send(error)
  }
});


module.exports = router;
