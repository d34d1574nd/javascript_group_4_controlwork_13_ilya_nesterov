const express = require('express');
const config = require('../config');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const ImagesPlace = require('../models/ImagesPlace');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.post('/', [auth, upload.array('images', 10)], async (req, res) => {
  try {
    const data = {
      images: '',
      userId: req.user._id,
      placeId: req.body.placeId
    }

    for (let i = 0; i < req.files.length; i++) {
      data.images = req.files[i].filename
      const imagesPlace = new ImagesPlace(data);
      imagesPlace.save()
    }
    
    return res.send({'message': "ok"})
  } catch (error) {
    console.log(error);
    return res.status(400).send(error)
  }
});


router.get('/:id', async (req, res) => {
  try {
    const images = await ImagesPlace.find({placeId: req.params.id});
    return res.send(images)
  } catch (error) {
    return res.status(400).send(error)
  }
});


module.exports = router;
