import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";

import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import AllPlacesCafe from "./containers/AllPlacesCafe/AllPlacesCafe";
import AddPlace from "./containers/AddPlace/AddPlace";
import AboutPlace from "./containers/AboutPlace/AboutPlace";

const ProtectedRoute = ({isAllowed, ...props}) => {
  return isAllowed ? <Route {...props} /> : <Redirect to="/login"/>
};

const Routes = ({user}) => {
  return (
    <Switch>
      <Route path="/" exact component={AllPlacesCafe}/>
      <Route path="/register" exact component={Register}/>
      <Route path="/login" exact component={Login}/>
      <ProtectedRoute isAllowed={user} path="/add_place" exact component={AddPlace}/>
      <Route path="/about_place/:id" exact component={AboutPlace}/>
    </Switch>
  );
};

export default Routes;
