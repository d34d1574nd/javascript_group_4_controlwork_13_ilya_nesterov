import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import StarRatings from "react-star-ratings";
import {Row, Button, Col, Form, FormGroup} from "reactstrap";
import {NotificationManager} from 'react-notifications';
import Slider from "react-slick";

import PlacesThumbnail from "../../components/PlacesThumbnail/PlacesThumbnail";
import FormElement from "../../components/UI/Form/FormElement";
import {fetchPlaces} from "../../store/actions/aboutPlaceAction";
import {addImages, fetchImages} from "../../store/actions/addImagesAction";
import {addComment, fetchComments} from "../../store/actions/addMessageAction";
import {addRatingForPlace} from "../../store/actions/addRatingForPlaceAction";
import {deleteComment, deleteImages} from "../../store/actions/adminDeleteAction";
import Spinner from "../../components/UI/Spinner/Spinner";
import {apiURL} from '../../constants';

import './AboutPlace.css';
import './slick.css';

class AboutPlace extends Component {
  _allRating;

  state = {
    comment: '',
    images: [],
    easyMakeRating: 0,
    quickMakeRating: 0,
    tasteRatings: 0,
  };

  componentDidMount() {
    this.props.fetchPlaces(this.props.match.params.id)
    this.props.fetchComments(this.props.match.params.id)
    this.props.fetchImages(this.props.match.params.id)
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.user) {
      if (this.props.comments !== prevProps.comments || this.props.images !== prevProps.images) {
        const data = {
          rating: this._allRating,
          comments: this.props.comments.length,
          images: this.props.images.length
        }
        this.props.addRatingForPlace(data, this.props.match.params.id)
      }
    }
  }

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  fileChangeHandler = event => {
    const fileValidation = /(.*?)\.(jpg|bmp|jpeg|png|img|svg)$/;
    if (event.target.value.match(fileValidation)) {
      this.setState({
        [event.target.name]: event.target.files
      })
    } else {
      NotificationManager.error('Не поддерживаемый формат файла')
    }
  };

  submitFormHandlerFiles = event => {
    event.preventDefault();

    const formData = new FormData();

    for (let i = 0; i < this.state.images.length; i++) {
      formData.append("images", this.state.images[i]);
    }
    formData.append("placeId", this.props.match.params.id)

    this.props.addImages(formData, this.props.match.params.id)
  };

  changeRating = (newRating, name) => {
    this.setState({
      [name]: newRating
    })
  };

  allRatings = () => {
    let easy = 0;
    let quick = 0;
    let taste = 0;
    this.props.comments.map(comment => {
      easy += comment.easyMakeRating / this.props.comments.length;
      quick += comment.quickMakeRating / this.props.comments.length;
      return taste += comment.tasteRatings / this.props.comments.length;
    });
    this._allRating = (easy + quick + taste)/3;
    return (
      <FormGroup row>
        <Col sm={3}>
          <span>Easy to make: </span>
          <StarRatings
            starDimension={'20px'}
            rating={this._allRating}
            starRatedColor="orange"
            numberOfStars={5}
          />
          <span> {this._allRating.toFixed(2)}</span>
        </Col>
        <Col sm={3}>
          <span>Easy to make: </span>
          <StarRatings
            starDimension={'20px'}
            rating={easy}
            starRatedColor="orange"
            numberOfStars={5}
          />
          <span> {easy.toFixed(2)}</span>
        </Col>
        <Col sm={3}>
          <span>Quick to make: </span>
          <StarRatings
            starDimension={'20px'}
            rating={quick}
            starRatedColor="orange"
            numberOfStars={5}
          />
          <span> {quick.toFixed(2)}</span>
        </Col>
        <Col sm={3}>
          <span>Taste rating:</span>
          <StarRatings
            starDimension={'20px'}
            rating={taste}
            starRatedColor="orange"
            numberOfStars={5}
          />
          <span> {taste.toFixed(2)}</span>
        </Col>
      </FormGroup>
    )
  }

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();

    for (let i = 0; i < this.state.images.length; i++) {
      formData.append("images", this.state.images[i]);
    }
    formData.append("message", this.state.comment)
    formData.append("easyMakeRating", this.state.easyMakeRating)
    formData.append("quickMakeRating", this.state.quickMakeRating)
    formData.append("tasteRatings", this.state.tasteRatings)
    formData.append("placeId", this.props.match.params.id)

    this.props.addComment(formData, this.props.match.params.id)
  };

  handleOnDragStart = e => e.preventDefault()

  render() {
    if (this.props.place === null) return null;
    if (this.props.comments === null) return null;
    if (this.props.images === null) return null;
    if (this.props.loading) {
      return <Spinner/>;
    }

    const settings = {
      dots: true,
      infinite: false,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 5000,
    };

    return (
        <Fragment>
          <Row className="placeOne">
            <Col sm={9}>
              <h4>{this.props.place.title}</h4>
              <p>{this.props.place.description}</p>
            </Col>
            <Col sm={3}>
              <PlacesThumbnail image={this.props.place.image} />
            </Col>
          </Row>
          <hr/>
          <div>
              <Fragment>
                <h3>Images</h3>
                <Slider {...settings}>

                  {this.props.images.map(image => (
                    <Fragment  key={image._id}>
                      {this.props.user && this.props.user.role === 'admin' ?
                        <Button color="danger" className="deleteImages"
                                onClick={() => this.props.deleteImages(image._id, this.props.match.params.id)}>
                          x
                        </Button>
                        : null}
                      <img src={`${apiURL}/uploads/${image.images}`} className="image"  alt="Places"/>
                    </Fragment>

                  ))}
                </Slider>
              </Fragment>
          </div>

          <div>
            <hr/>
            <h3>Average ratings</h3>
            {this.allRatings()}
          </div>
          <hr/>
          <div>
              <Fragment>
                <h3>Comments</h3>
                {this.props.comments.map((comment, index) => (
                  <div key={index} className="comments">
                    {this.props.user && this.props.user.role === 'admin' ?
                      <Button color="danger" className="deleteComment"
                              onClick={() => this.props.deleteComment(comment._id, this.props.match.params.id)}>
                        x
                      </Button>
                    : null}
                    <span>{comment.data} </span><span>{comment.userId.username}</span>
                    <p>{comment.message}</p>
                    <FormGroup row>
                      <Col sm={4}>
                        <span>Easy to make: </span>
                        <StarRatings
                          starDimension={'20px'}
                          rating={comment.easyMakeRating}
                          starRatedColor="orange"
                          numberOfStars={5}
                        />
                        <span className="Rating_about_place"> {comment.easyMakeRating}.0</span>
                      </Col>
                      <Col sm={4}>
                        <span>Quick to make: </span>
                        <StarRatings
                          starDimension={'20px'}
                          rating={comment.quickMakeRating}
                          starRatedColor="orange"
                          numberOfStars={5}
                        />
                        <span className="Rating_about_place"> {comment.quickMakeRating}.0</span>
                      </Col>
                      <Col sm={4}>
                        <span>Taste:</span>
                        <StarRatings
                          starDimension={'20px'}
                          rating={comment.tasteRatings}
                          starRatedColor="orange"
                          numberOfStars={5}
                        />
                        <span className="Rating_about_place"> {comment.tasteRatings}.0</span>
                      </Col>
                    </FormGroup>
                  </div>
                ))}
              </Fragment>
          </div>
          <hr/>
          {this.props.user ?
              <Form onSubmit={this.submitFormHandlerFiles}>
                <FormElement
                  propertyName="images"
                  title="images"
                  type="file" multiple required
                  onChange={this.fileChangeHandler}
                  text="Загрузите одну или несколько фотографий в формате: .png, .img, .jpeg, .bmp, .jpg"
                />
                {this.props.user && this.props.user._id === this.props.place.userId._id ?
                  <FormGroup row>
                    <Col sm={{offset: 3, size: 9}}>
                      <Button type="submit" color="primary">
                        Add photo
                      </Button>
                    </Col>
                  </FormGroup>
                  : null
                }
              </Form>
              : null}
          {this.props.user && this.props.user._id !== this.props.place.userId._id ?
            <Form onSubmit={this.submitFormHandler}>
              <FormElement
                propertyName="comment"
                title="comment" required
                type="textarea"
                value={this.state.comment}
                onChange={this.inputChangeHandler}
                placeholder="Enter comment"
              />
              <FormGroup row>
                <Col sm={{offset: 3, size: 9}}>
                  <FormGroup row>
                    <Col>
                      <p>easy to make</p>
                      <StarRatings
                        starDimension={'20px'}
                        rating={this.state.easyMakeRating}
                        starRatedColor="orange"
                        changeRating={this.changeRating}
                        numberOfStars={5}
                        name='easyMakeRating'
                      />
                    </Col>
                    <Col>
                      <p>quick to make</p>
                      <StarRatings
                        starDimension={'20px'}
                        rating={this.state.quickMakeRating}
                        starRatedColor="orange"
                        changeRating={this.changeRating}
                        numberOfStars={5}
                        name="quickMakeRating"
                      />
                    </Col>
                    <Col>
                      <p>taste</p>
                      <StarRatings
                        starDimension={'20px'}
                        rating={this.state.tasteRatings}
                        starRatedColor="orange"
                        changeRating={this.changeRating}
                        numberOfStars={5}
                        name="tasteRatings"
                      />
                    </Col>
                  </FormGroup>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col sm={{offset: 3, size: 9}}>
                  <Button type="submit" color="primary">
                    Add comment
                  </Button>
                </Col>
              </FormGroup>
            </Form>
            : null}
        </Fragment>

    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user,
  place: state.aboutPlace.place,
  loading: state.aboutPlace.loading,
  comments: state.comments.comments,
  images: state.images.images
});

const mapDispatchToProps = dispatch => ({
  fetchPlaces: id => dispatch(fetchPlaces(id)),
  addImages: (dataImages, id) => dispatch(addImages(dataImages, id)),
  addComment: (dataComment, id) => dispatch(addComment(dataComment, id)),
  fetchComments: id => dispatch(fetchComments(id)),
  fetchImages: id => dispatch(fetchImages(id)),
  addRatingForPlace: (rating, id) => dispatch(addRatingForPlace(rating, id)),
  deleteImages: (id, idPlace) => dispatch(deleteImages(id, idPlace)),
  deleteComment: (id, idPlace) => dispatch(deleteComment(id, idPlace)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AboutPlace);
