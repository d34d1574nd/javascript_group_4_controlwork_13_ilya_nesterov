import React, {Component} from 'react';
import {Button, Row, Col, Form, FormGroup, Input} from "reactstrap";
import {connect} from "react-redux";

import {addPlace} from "../../store/actions/allPlacesAction";
import FormElement from "../../components/UI/Form/FormElement";

import './AddPlace.css';

class AddPlace extends Component {
  state = {
    title: '',
    image: null,
    description: '',
    agreement: false
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    })
  };

  changeAgreement = () => {
    this.setState(prevState => ({
      agreement: !prevState.agreement
    }))
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();

    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.addPlace(formData);
  };

  render() {

    return (
      <div>
        <h3>Add new place</h3>
        <Form onSubmit={this.submitFormHandler}>
          <FormElement
            propertyName="title"
            title="title"
            type="text" required
            value={this.state.title}
            onChange={this.inputChangeHandler}
            placeholder="Enter title place"
          />

          <FormElement
            propertyName="image"
            title="image" multiple={false}
            type="file" required
            onChange={this.fileChangeHandler}
          />

          <FormElement
            propertyName="description"
            title="description"
            type="textarea" required
            value={this.state.description}
            onChange={this.inputChangeHandler}
            placeholder="Enter description"
          />

          <Row>
            <Col sm={3}>agreement</Col>
            <Col sm={9} className="checkBoxAgreement">
              <Input
                type="checkbox"
                onChange={this.changeAgreement}
              />
            </Col>
          </Row>

          <FormGroup row>
            <Col sm={{offset: 3, size: 9}}>
              <Button type="submit" color="primary">
                Add place
              </Button>
            </Col>
          </FormGroup>
        </Form>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  addPlace: data => dispatch(addPlace(data)),
});

export default connect(null, mapDispatchToProps)(AddPlace);
