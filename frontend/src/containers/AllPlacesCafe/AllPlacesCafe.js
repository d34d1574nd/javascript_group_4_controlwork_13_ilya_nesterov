import React, {Component, Fragment} from 'react';
import {NavLink as RouterNavLink} from "react-router-dom";
import {connect} from "react-redux";
import {Card, CardLink, Col, Row, NavLink, Button} from "reactstrap";
import StarRatings from "react-star-ratings";

import {fetchPlaces} from "../../store/actions/allPlacesAction";
import {deletePlace} from "../../store/actions/adminDeleteAction";

import Spinner from '../../components/UI/Spinner/Spinner';
import PlacesThumbnail from "../../components/PlacesThumbnail/PlacesThumbnail";

import './AllPlacesCafe.css';

class AllPlacesCafe extends Component {

  componentDidMount() {
    this.props.fetchPlaces()
  }

  render() {
    if (this.props.loading) {
      return <Spinner/>;
    }

    const places = this.props.places.map(place => {
      return <Col sm={3} key={place._id}>
        <Card body className="cardPlace">
          {this.props.user && this.props.user.role === "admin" ?
           <Button color="danger" className="deletePlace" onClick={() => this.props.deletePlace(place._id)}>x</Button>
          : null}
          <NavLink tag={RouterNavLink} rel="nofollow" className="about_place" to={`/about_place/${place._id}`}>
            <PlacesThumbnail image={place.image}/>
          </NavLink>
          <div className="text_about_place">
            <CardLink href={`/about_place/${place._id}`}>{place.title}</CardLink>
            <StarRatings
              starDimension={'20px'}
              rating={place.overallRatings}
              starRatedColor="orange"
              numberOfStars={5}
            />
            {place.overallRatings ?
              <Fragment>
                <p>({place.overallRatings && place.overallRatings.toFixed(2)}, {place.commentsAll} view)</p>
                <p> <i className = "fas fa-camera"/> {place.imagesAll} photos</p>
              </Fragment>
              : <div className="empty"></div>}
          </div>
        </Card>
      </Col>
    })
    return (
      <Row>
        {places}
      </Row>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user,
  places: state.places.places,
  loading: state.places.loading
});

const mapDispatchToProps = dispatch => ({
  fetchPlaces: () => dispatch(fetchPlaces()),
  deletePlace: id => dispatch(deletePlace(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(AllPlacesCafe);
