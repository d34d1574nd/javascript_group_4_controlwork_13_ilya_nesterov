import React from 'react';
import {NavLink as RouterNavLink} from "react-router-dom";
import {NavLink, Navbar} from "reactstrap";

const UserMenu = ({user, logout}) => {
  return (
    <Navbar>
      <NavLink tag={RouterNavLink} rel="nofollow" to={'/'}
                       className="user username">Hello, {user.username}</NavLink>
      <NavLink tag={RouterNavLink} rel="nofollow" to={'/add_place'}
               className="user username">Add place</NavLink>
      <NavLink tag={RouterNavLink} rel="nofollow" to="/" className="user" onClick={logout}>LogOut</NavLink>
    </Navbar>
  )
};

export default UserMenu;
