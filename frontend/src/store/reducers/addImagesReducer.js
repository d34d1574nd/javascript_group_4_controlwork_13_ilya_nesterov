import {FETCH_IMAGES_FOR_PLACE} from "../actions/addImagesAction";

const initialState = {
  images: []
};

const aboutPlaceReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_IMAGES_FOR_PLACE:
      return {...state, images: action.images}
    default:
      return state;
  }
};

export default aboutPlaceReducer;
