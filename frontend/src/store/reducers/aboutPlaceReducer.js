import {FETCH_ONE_PLACE_SUCCESS, LOADING_ONE_PLACE_SUCCESS} from "../actions/aboutPlaceAction";

const initialState = {
  place: null,
  loading: true
};

const aboutPlaceReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ONE_PLACE_SUCCESS:
      return {...state, place: action.place};
    case LOADING_ONE_PLACE_SUCCESS:
      return {...state, loading: action.cancel};
    default:
      return state;
  }
};

export default aboutPlaceReducer;
