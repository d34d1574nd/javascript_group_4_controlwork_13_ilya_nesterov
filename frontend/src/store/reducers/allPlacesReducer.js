import {FETCH_ALL_PLACES_SUCCESS, LOADING_ALL_PLACES_SUCCESS} from "../actions/allPlacesAction";

const initialState = {
  places: [],
  loading: true
};

const allPlacesReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ALL_PLACES_SUCCESS:
      return {...state, places: action.places};
    case LOADING_ALL_PLACES_SUCCESS:
      return {...state, loading: action.cancel};
    default:
      return state;
  }
};

export default allPlacesReducer;
