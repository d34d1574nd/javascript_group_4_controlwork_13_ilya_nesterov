import {FETCH_COMMENTS_FOR_PLACE} from "../actions/addMessageAction";

const initialState = {
  comments: []
};

const aboutPlaceReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_COMMENTS_FOR_PLACE:
      return {...state, comments: action.comments}
    default:
      return state;
  }
};

export default aboutPlaceReducer;
