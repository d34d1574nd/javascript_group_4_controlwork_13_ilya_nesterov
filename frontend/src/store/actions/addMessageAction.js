import axios from '../../axios-api';
import {push} from 'connected-react-router';

export const FETCH_COMMENTS_FOR_PLACE = 'FETCH_COMMENTS_FOR_PLACE';

const fetchCommentsSuccess = comments => ({type: FETCH_COMMENTS_FOR_PLACE, comments});


export const addComment = (dataComment, id) => {
  return dispatch => {
    return axios.post(`/comments`, dataComment).then(
      () => {
        dispatch(fetchComments(id));
        dispatch(push(`/about_place/${id}`));
      }
    )
  }
};

export const fetchComments = id => {
  return dispatch => {
    return axios.get(`/comments/${id}`).then(
      response => {
        dispatch(fetchCommentsSuccess(response.data));
        dispatch(push(`/about_place/${id}`));
      }
    )
  }
}



