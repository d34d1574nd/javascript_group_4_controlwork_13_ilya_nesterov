import {push} from 'connected-react-router';
import axios from '../../axios-api';
import {NotificationManager} from 'react-notifications';

export const FETCH_ALL_PLACES_SUCCESS = 'FETCH_ALL_PLACES_SUCCESS';
export const LOADING_ALL_PLACES_SUCCESS = 'LOADING_ALL_PLACES_SUCCESS';

const fetchPlacesSuccess = places => ({type: FETCH_ALL_PLACES_SUCCESS, places});
const loadingPlaces = cancel => ({type: LOADING_ALL_PLACES_SUCCESS, cancel});

export const fetchPlaces = () => {
  return dispatch => {
    return axios.get('/places').then(
      response => {
        dispatch(fetchPlacesSuccess(response.data));
        dispatch(push('/'));
      }
    ).finally(() => dispatch(loadingPlaces(false)))
  }
};


export const addPlace = data => {
  return dispatch => {
    return axios.post('/places', data).then(
      response => {
        dispatch(fetchPlaces())
        NotificationManager.success('Заведение создано')
        dispatch(push('/'));
      },
      error => {
        NotificationManager.error("Вы не приняли пользовательское соглашение")
      }
    )
  }
};
