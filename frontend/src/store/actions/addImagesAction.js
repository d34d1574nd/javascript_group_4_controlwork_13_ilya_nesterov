import axios from '../../axios-api';
import {push} from 'connected-react-router';

import {fetchPlaces} from "./aboutPlaceAction";

export const FETCH_IMAGES_FOR_PLACE = 'FETCH_IMAGES_FOR_PLACE';

const fetchImagesSuccess = images => ({type: FETCH_IMAGES_FOR_PLACE, images});


export const fetchImages = id => {
  return dispatch => {
    return axios.get(`/images/${id}`).then(
      response => {
        dispatch(fetchImagesSuccess(response.data))
        dispatch(push(`/about_place/${id}`));
      }
    )
  }
}

export const addImages = (dataImages, id) => {
  return dispatch => {
    return axios.post(`/images`, dataImages).then(
      () => {
        dispatch(fetchPlaces(id));
        dispatch(fetchImages(id));
        dispatch(push(`/about_place/${id}`));
      }
    )
  }
};

