import axios from '../../axios-api';

export const FETCH_ONE_PLACE_SUCCESS = 'FETCH_ONE_PLACE_SUCCESS';
export const LOADING_ONE_PLACE_SUCCESS = 'LOADING_ONE_PLACE_SUCCESS';

const fetchOnePlaceSuccess = place => ({type: FETCH_ONE_PLACE_SUCCESS, place});
const loadingPlace = cancel => ({type: LOADING_ONE_PLACE_SUCCESS, cancel});

export const fetchPlaces = id => {
  return dispatch => {
    return axios.get(`/places/${id}`).then(
      response => {
        dispatch(fetchOnePlaceSuccess(response.data));
      }
    ).finally(() => dispatch(loadingPlace(false)))
  }
};
