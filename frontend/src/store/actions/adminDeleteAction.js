import axios from '../../axios-api';
import {NotificationManager} from 'react-notifications';

import {fetchComments} from './addMessageAction';
import {fetchImages} from './addImagesAction';
import {fetchPlaces} from './allPlacesAction';

export const deleteComment = (id, idPlace) => {
  return dispatch => {
    return axios.delete(`/admin/comment/${id}`).then(
      () => {
        dispatch(fetchComments(idPlace));
        NotificationManager.success('Комментарий успешно удален')
      }
    )
  }
};
export const deleteImages = (id, idPlace) => {
  return dispatch => {
    return axios.delete(`/admin/image/${id}`).then(
      () => {
        dispatch(fetchImages(idPlace));
        NotificationManager.success('Фотография успешно удалена')
      }
    )
  }
};
export const deletePlace = id => {
  return dispatch => {
    return axios.delete(`/admin/place/${id}`).then(
      () => {
        dispatch(fetchPlaces());
        NotificationManager.success('Заведение успешно удалено')
      }
    )
  }
};
