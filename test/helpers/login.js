
const Helper = codeceptjs.helper;

const userData = {
  'User': {
    'username': 'user',
    'password': '123'
  },
  'Ilya': {
    'username': 'Ilya',
    'password': '123'
  },
  'Admin': {
    'username': 'admin',
    'password': '123'
    },
};



class LoginHelper extends Helper {

  async loginAsUser(name) {
    const user = userData[name];

    if (!user) throw new Error('No such user is known! Check helpers/login.js');

    const I = this.helpers['Puppeteer'];

    await I.amOnPage('login');

    const alreadyLoggedIn = await I._locate(`//a[text()='Hello, ${name}']`);

    if (alreadyLoggedIn.length > 0) {
      return;
    }

    const someoneIsLoggedIn = await I._locate(`//a[text()='Hello, ']`);

    if (someoneIsLoggedIn.length > 0) {
      await I.click(`//a[contains(., 'Hello, ')]`);
      await I.click(`//a[text()='LogOut']`);
    }


    await I.fillField(`//input[@id='username']`, user.username);
    await I.fillField(`//input[@id='password']`, user.password);

    await I.click(`//button[text()='Login']`);

    await I.waitForText(`Успешно авторизовались`, 2);
  }
}

module.exports = LoginHelper;
