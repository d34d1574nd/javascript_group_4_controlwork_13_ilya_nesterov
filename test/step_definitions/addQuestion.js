const {I} = inject();
// Add in your custom step files

When('нажимаю на кнопку и перехожу на ссылку создания заведения' , () => {
  I.waitForVisible(`//a[(.='Add place')]`, 10);
  I.click(`//a[contains(text(),'Add place')]`);
  I.amOnPage('add_place')
})

When('я заполняю данные в форме вопроса и {string} галочку', (button, text) => {
  I.waitForVisible(`//h3[contains(text(),'Add new place')]`, 10);
  I.fillField(`//input[@id='title']`, "Бублик2");
  I.attachFile(`//input[@id='image']`, 'files/bublik.jpeg');
  I.fillField(`//textarea[@id='description']`, "Описание бублика");
  if (button === "ставлю") {
    I.click(`//input[@class='form-check-input']`);
    I.click(`//button[@class='btn btn-primary']`);
  } else if (button === "не ставлю") {
    I.click(`//button[@class='btn btn-primary']`);
  }
});

Then('я вижу ответ от сервера {string}', text => {
  I.waitForText(text);
})

Then('я вижу ответ от сервера и такое заведение на странице', () => {
  I.waitForText("Заведение создано");
  I.waitForVisible(`//a[.='Бублик2']`);
})
