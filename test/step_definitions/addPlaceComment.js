const {I} = inject();
// Add in your custom step files

When('я захожу на страницу заведения Бублик', () => {
  I.click(`//a[.='Бублик2']`);
  I.seeElement(`//h4[.='Бублик2']`);
})

When('я заполняю данные в форме комментария с рейтингом', () => {
  I.fillField(`//textarea[@id='comment']`, 'Хороший кофе');
  I.click(`//form//div[@class='row form-group']//div[1]//div[1]//div[3]`);
  I.click(`//form//div[@class='row form-group']//div[2]//div[1]//div[2]`);
  I.click(`//form//div[@class='row form-group']//div[3]//div[1]//div[4]`);
  I.click(`//button[.='Add comment']`);
})

Then('я вижу мой комментарий и измененный рейтинг к этому заведению', () => {
  I.waitForElement(`//span[contains(text(),'Ilya')]`);
  I.seeElement(`//h3[contains(text(),'Average ratings')]`);
  I.seeElement(`//body/div[@id='root']/div[@class='container']/div/div[@class='row form-group']/div[2]/div[1]/div[3]`);
  I.seeElement(`//body/div[@id='root']/div[@class='container']/div/div[@class='row form-group']/div[3]/div[1]/div[2]`);
  I.seeElement(`//body/div[@id='root']/div[@class='container']/div/div[@class='row form-group']/div[4]/div[1]/div[4]`);
  I.seeElement(`//body/div[@id='root']/div[@class='container']/div/div[@class='row form-group']/div[1]/div[1]/div[3]`);
})
